program TestUG;

{$APPTYPE CONSOLE}

//{$R *.res}

uses
  Winapi.Windows,
  System.SysUtils,
  System.Classes,
  System.Generics.Collections,
  LockFree in 'LockFree.pas';

type
  // �����, ���������� �������� � �������
  TEmissionThread = class(TThread)
  protected
    procedure Execute; override;
  end;


var
  i: Integer;
  Threads: TObjectList<TThread>;

  Q: TSyncQueue<Integer>;

procedure TEmissionThread.Execute;
var
  Value: Integer;
begin
  while (not Terminated) do
  begin
    Q.Enqueue(3);
    if not Q.Dequeue(Value) then
      raise Exception.Create('Error Message');
  end;
end;

begin
  try
    Q := TSyncQueue<Integer>.Create();
    try
      Threads:= TObjectList<TThread>.Create;
      try
        // �������������� ������
        for i := 1 to 5 do
          Threads.Add(TEmissionThread.Create);

        Writeln;
        Write('Press Enter to quit');
        Readln;
      finally
        Threads.Free;
      end;
    finally
      Q.Free;
    end;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
