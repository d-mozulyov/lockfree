program CheckProject;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  Winapi.Windows,
  System.SysUtils;

function CaptureConsoleOutput(const ACommand, AParameters: string): string;
const
  CReadBuffer = 2400;
var
  saSecurity: TSecurityAttributes;
  hRead: THandle;
  hWrite: THandle;
  suiStartup: TStartupInfo;
  piProcess: TProcessInformation;
  Buffer: array [0..CReadBuffer] of AnsiChar;
  Len: Integer;
  dRead: DWORD;
  dAvailable: DWORD;
begin
  Result := '';

  saSecurity.nLength := SizeOf(TSecurityAttributes);
  saSecurity.bInheritHandle := true;
  saSecurity.lpSecurityDescriptor := nil;
  if CreatePipe(hRead, hWrite, @saSecurity, 0) then
    try
      FillChar(suiStartup, SizeOf(TStartupInfo), #0);
      suiStartup.cb := SizeOf(TStartupInfo);
      suiStartup.hStdInput := hRead;
      suiStartup.hStdOutput := hWrite;
      suiStartup.hStdError := hWrite;
      suiStartup.dwFlags := STARTF_USESTDHANDLES or STARTF_USESHOWWINDOW;
      suiStartup.wShowWindow := SW_HIDE;
      if CreateProcess(nil, PChar(ACommand + ' ' + AParameters), @saSecurity, @saSecurity, true, NORMAL_PRIORITY_CLASS, nil, nil, suiStartup,
        piProcess) then
        try
          WaitForSingleObject(piProcess.hProcess, INFINITE);
          PeekNamedPipe(hRead, nil, 0, nil, @dAvailable, nil);
          if (dAvailable > 0) then
            repeat
              dRead := 0;
              ReadFile(hRead, Buffer, CReadBuffer, dRead, nil);

              Len := Length(Result);
              SetLength(Result, Len + Integer(dRead));
              MultiByteToWideChar(0, 0, Buffer, dRead, PChar(Pointer(Result)) + Len, dRead);
            until (dRead < CReadBuffer);
        finally
          CloseHandle(piProcess.hProcess);
          CloseHandle(piProcess.hThread);
        end;
    finally
      CloseHandle(hRead);
      CloseHandle(hWrite);
    end;
end;

var
  i: Integer;
  S: string;

begin
  try
    for i := 1 to 10000 do
    begin
      Write('Iteration ', i, '... ');

      S := CaptureConsoleOutput('Project.exe', '-nowait');
      if (S = '') then
      begin
        Writeln('done');
      end else
      begin
        Writeln;
        Writeln(S);
        Break;
      end;
    end;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;

  Writeln;
  Write('Press Enter to quit');
  Readln;
end.
