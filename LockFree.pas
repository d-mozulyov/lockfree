unit LockFree;

{$LEGACYIFEND ON}
{$WEAKLINKRTTI ON}
{$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$U-}{$V+}{$B-}{$X+}{$T+}{$P+}{$H+}{$J-}{$Z1}{$A4}{$O+}{$R-}{$I-}{$Q-}{$W-}

{$if Defined(CPUX64) or Defined(CPUARM64)}
  {$define LARGEINT}
{$else}
  {$define SMALLINT}
{$ifend}

interface
  uses {$ifdef MSWINDOWS}Winapi.Windows,{$else .POSIX}Posix.PThread,{$endif}
       System.SysUtils;

type
  // ����� ����������� 8-�������� ���������, ��������������� ��� �������� "tagged pointer"
  // ��� ���� 8 ����, ������� ���� ����� �������� ����� AtomicCmpExchange
  // ������� ����� lock-free ���������� �������� �� ����� "���������"
  // �����: ���������� � ������������� �� 8 ���� ���� ��������������
  PSyncPointer = ^TSyncPointer;
  TSyncPointer = packed record
  private
    {$if CompilerVersion >= 29}[Volatile]{$ifend}
    F: packed record
    case Integer of
      0: (Value: Pointer);
      1: (VLow, VHigh: Integer);
      2: (VInt64: Int64);
      3: (VNative: NativeUInt);
    end;

    function GetEmpty: Boolean; inline;
    {$ifdef CPUX64}
    function GetValue: Pointer; inline;
    procedure SetValue(const AValue: Pointer); inline;
    {$endif}

    {$ifdef CPUX64}
    const
      X64_SYNCPTR_MASK = ((NativeUInt(1) shl 48) - 1) and -8;
      X64_SYNCPTR_CLEAR = not X64_SYNCPTR_MASK;
    {$endif}
  public
    // ������� ������ � ��������
    class function Create(const AValue: Pointer): TSyncPointer; overload; static; inline;
    class function Create(const AValue: Int64): TSyncPointer; overload; static; inline;
    class function Create(const ALow, AHigh: Integer): TSyncPointer; overload; static; inline;
    class operator Equal(const a, b: TSyncPointer): Boolean; inline;

    function Copy: TSyncPointer; inline;
    procedure Fill(const AValue: Pointer); overload; {$ifNdef CPUX86}inline;{$endif}
    procedure Fill(const AValue: TSyncPointer); overload; {$ifNdef CPUX86}inline;{$endif}
    function AtomicCmpExchange(const NewValue: Pointer; const Comparand: TSyncPointer): Boolean; overload;
    function AtomicCmpExchange(const NewValue: TSyncPointer; const Comparand: TSyncPointer): Boolean; overload;
    function AtomicExchange(const NewValue: Pointer): TSyncPointer; overload;
    function AtomicExchange(const NewValue: TSyncPointer): TSyncPointer; overload;

    property Empty: Boolean read GetEmpty;
    {$ifdef CPUX64}
    property Value: Pointer read GetValue write SetValue;
    {$else}
    property Value: Pointer read F.Value write F.Value;
    {$endif}
  public
    // ����
    procedure Push(const Value: Pointer); inline;
    function Pop: Pointer;

    procedure PushList(const First, Last: Pointer); overload;
    procedure PushList(const First: Pointer{Last calculated}); overload;
    function PopList: Pointer;
  end;


  // ��� ��� ����� ��������� ���������� � ������ ��������� lock-free ��������
  TSyncYield = packed record
  private
    FCount: Byte;
  public
    class function Create: TSyncYield; static; inline;
    procedure Reset; inline;
    procedure Execute;
  end;


  // ������ ����� ��������� ���, ������ �������� ����� 0
  TNothing = packed record
  end;


  // ����� �������� ����� � �����������
  // ������� �� ��������� �� ���������, ��������� � ������ ��������
  TAllocatorNode<TNext,THeader,TValue> = packed record
    Next: TNext;
    Header: THeader;
    Value: TValue;
  end;


  // �����, ������ � ������� ��������� �� ������� 8 ����
  // �� ������ ������ ����� ������������ ����������
  TSyncObject = class(TInterfacedObject)
  private
    {$ifdef SMALLINT}
    {$HINTS OFF}
    _Padding: Cardinal;
    {$HINTS ON}
    {$endif}
  end;


  // ��� ��������������� ������� � ������
  // ����������� ��� ������������� � ���������� �������������
  TSyncHelpers = record
  private
    type
      PEmpties = ^TEmpties;
      TEmpties = packed record
      const
        ALIGN = SizeOf(TSyncPointer);
        BASE_ALIGN = 16;

        MAX_SIZE = 256;
        MAX_SLOTS = MAX_SIZE div ALIGN;
        MAX_COUNT_SHIFT = 10{1024 items};
        LARGE_COUNT = 64;
      public
        class var
          PoolsBuffer: array[0..SizeOf(Pointer) + SizeOf(Pointer) - 1] of Byte;

        class function GetPools: PPointer; static; inline;
        class function AllocPool(var SyncPointer: TSyncPointer; ItemSize, Count: Integer): Pointer; overload; static;
        class procedure Finalize; static;
      public
        Slots: array[0..MAX_SLOTS] of TSyncPointer;
        CountShifts: array[0..MAX_SLOTS] of Integer;

        function AllocPool(const ItemSize, Index: Integer): Pointer; overload;
      end;
      TEmptiesBuffer = array[0..SizeOf(TEmpties) + TEmpties.ALIGN - 1] of Byte;

      TEmptiesSlotCache = packed record
        Temp: TSyncPointer;
        Value: PSyncPointer;
      end;
      PEmptiesSlotCache = ^TEmptiesSlotCache;

    class var
      FCommonEmpties: TEmptiesBuffer;
      FSyncPointerEmpties: TEmptiesBuffer;

    class function GetEmpties(const ASyncPointer: Boolean): PEmpties; static;
  public
    class destructor Destroy;
  end;


  // lock-free ��������� �������� ������
  // �� �������� �� ����������� New � Dispose
  // ������� ������� ������� � ���, ��� �� ����� ���� ��������� �� ���������
  // � ��� ����� ���������� �������� �������� ������
  TSyncAllocator<T> = record
    class function New: Pointer; static; inline;
    class procedure Dispose(const X: Pointer); static; inline;
  end;
  TSyncAllocator<TNext,THeader,TValue> = record
  private
    class var
      FEmptiesSlotCache: array[0..SizeOf(TSyncHelpers.TEmptiesSlotCache) + TSyncHelpers.TEmpties.ALIGN - 1] of Byte;

    class function InternalGetEmptiesSlot: PSyncPointer; static;
    class function GetEmptiesSlot: PSyncPointer; static; inline;
    class function AddEmptiesPool: Pointer; static;

    class property EmptiesSlot: PSyncPointer read GetEmptiesSlot;
  public
    type
      TNode = TAllocatorNode<TNext,THeader,TValue>;
      PNode = ^TNode;

    class function New: Pointer; static;
    class procedure Dispose(const X: Pointer); static;
  end;


  // lock-free ����
  ISyncStack<T> = interface
    procedure Clear;
    procedure Push(const Value: T); overload;
    function Pop(var Value: T): Boolean;
    procedure Push(const Values: array of T); overload;
    function PopAll: TArray<T>;
    function GetEmpty: Boolean;

    property Empty: Boolean read GetEmpty;
  end;

  TSyncStack<T> = class(TSyncObject, ISyncStack<T>)
  private
    FHead: TSyncPointer;

    type
      TNode = TAllocatorNode<Pointer,TNothing,T>;
      PNode = ^TNode;

    function GetEmpty: Boolean; inline;
  public
    constructor Create;
    destructor Destroy; override;

    procedure Clear;
    procedure Push(const Value: T); overload;
    function Pop(var Value: T): Boolean;

    procedure Push(const Values: array of T); overload;
    function PopAll: TArray<T>;

    property Empty: Boolean read GetEmpty;
    //property Counting: Boolean read FCounting;
    //property Count: Integer read GetCount;
  end;


  // lock-free �������
  ISyncQueue<T> = interface
    procedure Clear;
    function Enqueue(const Value: T): Boolean;
    function Dequeue(var Value: T): Boolean;
    function GetEmpty: Boolean;

    property Empty: Boolean read GetEmpty;
  end;

  TSyncQueue<T> = class(TSyncObject, ISyncQueue<T>)
  private
    FHead: TSyncPointer;
    FTail: TSyncPointer;

    type
      TNode = TAllocatorNode<Pointer,TNothing,T>;
      PNode = ^TNode;

    function GetEmpty: Boolean; inline;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;

    function Enqueue(const Value: T): Boolean;
    function Dequeue(var Value: T): Boolean;

    property Empty: Boolean read GetEmpty;
  end;


implementation


{ TSyncPointer }

class function TSyncPointer.Create(const AValue: Pointer): TSyncPointer;
begin
  Result.F.Value := AValue;
  {$ifdef SMALLINT}
    Result.F.VHigh := 0;
  {$endif}
end;

class function TSyncPointer.Create(const AValue: Int64): TSyncPointer;
begin
  Result.F.VInt64 := AValue;
end;

class function TSyncPointer.Create(const ALow, AHigh: Integer): TSyncPointer;
begin
  Result.F.VLow := ALow;
  Result.F.VHigh := AHigh;
end;

class operator TSyncPointer.Equal(const a, b: TSyncPointer): Boolean;
begin
  Result := (Int64(a) = Int64(b));
end;

function TSyncPointer.Copy: TSyncPointer;
begin
  {$if Defined(LARGEINT)}
    Result.F.VInt64 := Self.F.VInt64;
  {$elseif Defined(CPUX86)}
    repeat
      Result.F.Value := Self.F.Value;
      Result.F.VHigh := Self.F.VHigh;
    until (Result.F.Value = Self.F.Value);
  {$else .CPUARM32}
    Result.F.Value := Self.F.Value;
    Result.F.VHigh := 0;
  {$ifend}
end;

procedure TSyncPointer.Fill(const AValue: Pointer);
{$ifNdef CPUX86}
begin
  Self.F.VNative := NativeUInt(AValue)
    {$ifdef CPUX64}+ (((Self.F.VNative or X64_SYNCPTR_MASK) + 1) and X64_SYNCPTR_CLEAR){$endif};
end;
{$else .CPUX86}
begin
  Self.Fill(TSyncPointer.Create(Integer(AValue), Self.F.VHigh + 1));
end;
{$endif}

procedure TSyncPointer.Fill(const AValue: TSyncPointer);
{$if Defined(LARGEINT)}
begin
  F.VInt64 := AValue.F.VInt64;
end;
{$elseif not Defined(CPUX86)}
begin
  System.AtomicExchange(Self.F.VInt64, AValue.F.VInt64);
end;
{$else}
asm
  fild qword ptr [edx]
  fistp qword ptr [eax]
end;
{$ifend}

function TSyncPointer.GetEmpty: Boolean;
begin
  {$ifdef CPUX64}
    Result := (Self.F.VNative and X64_SYNCPTR_MASK = 0);
  {$else .CPUARM64}
    Result := (Self.F.Value = nil);
  {$endif}
end;

{$ifdef CPUX64}
function TSyncPointer.GetValue: Pointer;
begin
  Result := Pointer(Self.F.VNative and X64_SYNCPTR_MASK);
end;

procedure TSyncPointer.SetValue(const AValue: Pointer);
begin
  Self.F.VNative := (Self.F.VNative and X64_SYNCPTR_CLEAR) or NativeUInt(AValue);
end;
{$endif}

function TSyncPointer.AtomicCmpExchange(const NewValue: Pointer;
  const Comparand: TSyncPointer): Boolean;
var
  _NewValue: TSyncPointer;
begin
  _NewValue.F.VNative := NativeUInt(NewValue)
    {$ifdef CPUX64}+ (((Comparand.F.VNative or X64_SYNCPTR_MASK) + 1) and X64_SYNCPTR_CLEAR){$endif};
  {$ifdef CPUX86}
  _NewValue.F.VHigh := Comparand.F.VHigh + 1;
  {$endif}
  {$ifdef CPUARM32}
  _NewValue.F.VHigh := 0;
  {$endif}

  Result := (Self.F.VInt64 = Comparand.F.VInt64) and
    (System.AtomicCmpExchange(Self.F.VInt64, _NewValue.F.VInt64, Comparand.F.VInt64) = Comparand.F.VInt64);
end;

function TSyncPointer.AtomicCmpExchange(const NewValue: TSyncPointer;
  const Comparand: TSyncPointer): Boolean;
begin
  Result := (Self.F.VInt64 = Comparand.F.VInt64) and
    (System.AtomicCmpExchange(Self.F.VInt64, NewValue.F.VInt64, Comparand.F.VInt64) = Comparand.F.VInt64);
end;

function TSyncPointer.AtomicExchange(const NewValue: Pointer): TSyncPointer;
var
  _NewValue: TSyncPointer;
begin
  {$if Defined(CPUX86)}
    _NewValue.F.Value := NewValue;
    _NewValue.F.VHigh := Self.F.VHigh + 1;
  {$elseif Defined(CPUX64)}
    _NewValue.F.VNative := NativeUInt(NewValue) +
      (((Self.F.VNative or X64_SYNCPTR_MASK) + 1) and X64_SYNCPTR_CLEAR);
  {$else}
    _NewValue.F.Value := NewValue;
    {$ifdef SMALLINT}
      _NewValue.F.VHigh := 0;
    {$endif}
  {$ifend}

  Result.F.VInt64 := System.AtomicExchange(Self.F.VInt64, _NewValue.F.VInt64);
end;

function TSyncPointer.AtomicExchange(const NewValue: TSyncPointer): TSyncPointer;
begin
  Result.F.VInt64 := System.AtomicExchange(Self.F.VInt64, NewValue.F.VInt64);
end;

procedure TSyncPointer.Push(const Value: Pointer);
begin
  Self.PushList(Value, Value);
end;

procedure TSyncPointer.PushList(const First, Last: Pointer);
var
  Head: TSyncPointer;
begin
  repeat
    Head := Self.Copy;
    PPointer(Last)^ := Head.Value;
  until (Self.AtomicCmpExchange(First, Head));
end;

procedure TSyncPointer.PushList(const First: Pointer{Last calculated});
var
  Last, Next: Pointer;
begin
  Next := PPointer(First)^;
  Last := First;

  if (Next <> nil) then
  repeat
    Last := Next;
    Next := PPointer(Next)^;
  until (Next = nil);

  Self.PushList(First, Last);
end;

function TSyncPointer.Pop: Pointer;
var
  Head: TSyncPointer;
begin
  repeat
    Head := Self.Copy;
    Result := Head.Value;
  until (Result = nil) or (Self.AtomicCmpExchange(PPointer(Result)^, Head));
end;

function TSyncPointer.PopList: Pointer;
var
  Head: TSyncPointer;
begin
  repeat
    Head := Self.Copy;
    Result := Head.Value;
  until (Result = nil) or (Self.AtomicCmpExchange(nil, Head));
end;


{ TSyncYield }

class function TSyncYield.Create: TSyncYield;
begin
  Result.FCount := High(Byte);
end;

procedure TSyncYield.Reset;
begin
  Self.FCount := High(Byte);
end;

procedure TSyncYield.Execute;
var
  Count: Integer;
begin
  Count := (Integer(FCount) + 1) and 7;
  FCount := Count;

  case Count of
    0..4: System.YieldProcessor;
    5, 6:
    begin
      {$ifdef MSWINDOWS}
        SwitchToThread;
      {$else .POSIX}
        sched_yield;
      {$endif}
    end;
  else
    Sleep(1);
  end;
end;


{ TSyncHelpers.TEmpties }

class function TSyncHelpers.TEmpties.GetPools: PPointer;
begin
  Result := Pointer(@TSyncHelpers.TEmpties.PoolsBuffer[SizeOf(Pointer) - 1]);
  Result := Pointer(NativeInt(Result) and -SizeOf(Pointer));
end;

class function TSyncHelpers.TEmpties.AllocPool(var SyncPointer: TSyncPointer;
  ItemSize, Count: Integer): Pointer;
var
  i: Integer;
  Value, Next, Last: Pointer;
begin
  ItemSize := (ItemSize + ALIGN - 1) and -ALIGN;
  GetMem(Value, SizeOf(Pointer) + BASE_ALIGN - 1 + ItemSize * Count);
  repeat
    Next := TSyncHelpers.TEmpties.GetPools^;
    PPointer(Value)^ := Next;
    if (AtomicCmpExchange(TSyncHelpers.TEmpties.GetPools^, Value, Next) = Next) then Break;
  until (False);

  Result := Pointer((NativeInt(Value) + SizeOf(Pointer) + BASE_ALIGN - 1) and -BASE_ALIGN);
  Last := Pointer(NativeInt(Result) + ItemSize);
  for i := 1 to Count - 2 do
  begin
    Next := Pointer(NativeInt(Last) + ItemSize);
    PPointer(Last)^ := Next;
    Last := Next;
  end;
  PPointer(Last)^ := nil;
  SyncPointer.PushList(Pointer(NativeInt(Result) + ItemSize), Last);
end;

function TSyncHelpers.TEmpties.AllocPool(const ItemSize, Index: Integer): Pointer;
var
  Shift: Integer;
begin
  repeat
    if (Self.CountShifts[Index] = 0) then
    begin
      Shift := 2;
      if (AtomicCmpExchange(Self.CountShifts[Index], Shift, 0) = 0) then Break;
    end else
    begin
      Shift := AtomicIncrement(Self.CountShifts[Index]);
      if (Shift > MAX_COUNT_SHIFT) then
      begin
        AtomicDecrement(Self.CountShifts[Index]);
        Shift := MAX_COUNT_SHIFT;
      end;
      Break;
    end;
  until (False);

  Result := TSyncHelpers.TEmpties.AllocPool(Self.Slots[Index], ItemSize, 1 shl Shift);
end;

class procedure TSyncHelpers.TEmpties.Finalize;
var
  Item, Next: Pointer;
begin
  repeat
    Item := TSyncHelpers.TEmpties.GetPools^;
    if (Item = nil) then Break;
    if (AtomicCmpExchange(TSyncHelpers.TEmpties.GetPools^, nil, Item) = Item) then Break;
  until (False);

  while (Item <> nil) do
  begin
    Next := PPointer(Item)^;
    FreeMem(Item);
    Item := Next;
  end;
end;

{ TSyncHelpers }

class destructor TSyncHelpers.Destroy;
begin
  TSyncHelpers.TEmpties.Finalize;
end;

class function TSyncHelpers.GetEmpties(const ASyncPointer: Boolean): PEmpties;
begin
  if (not ASyncPointer) then
  begin
    Result := Pointer(@FCommonEmpties[TEmpties.ALIGN - 1]);
  end else
  begin
    Result := Pointer(@FSyncPointerEmpties[TEmpties.ALIGN - 1]);
  end;

  Result := Pointer(NativeInt(Result) and -TEmpties.ALIGN);
end;


 { TSyncAllocator<T> }

class function TSyncAllocator<T>.New: Pointer;
begin
  Result := TSyncAllocator<TNothing,TNothing,T>.New;
end;

class procedure TSyncAllocator<T>.Dispose(const X: Pointer);
begin
  TSyncAllocator<TNothing,TNothing,T>.Dispose(X);
end;


{ TSyncAllocator<THeader,TValue> }

class function TSyncAllocator<TNext,THeader,TValue>.InternalGetEmptiesSlot: PSyncPointer;
var
  HighAddress: NativeInt;
  Index: NativeUInt;
  Cache: TSyncHelpers.PEmptiesSlotCache;
begin
  HighAddress := NativeInt(@FEmptiesSlotCache[TSyncHelpers.TEmpties.ALIGN - 1]);
  Cache := TSyncHelpers.PEmptiesSlotCache(HighAddress and -TSyncHelpers.TEmpties.ALIGN);

  if (TypeInfo(TNext) = TypeInfo(TSyncPointer)) and
    (SizeOf(THeader) + SizeOf(TValue) <= TSyncHelpers.TEmpties.MAX_SIZE) then
  begin
    Index := (SizeOf(THeader) + SizeOf(TValue) + TSyncHelpers.TEmpties.ALIGN - 1) div TSyncHelpers.TEmpties.ALIGN;
    Cache.Value := @TSyncHelpers.GetEmpties(True).Slots[Index];
  end else
  if (SizeOf(TNode) <= TSyncHelpers.TEmpties.MAX_SIZE) then
  begin
    Index := (SizeOf(TNode) + TSyncHelpers.TEmpties.ALIGN - 1) div TSyncHelpers.TEmpties.ALIGN;
    Cache.Value := @TSyncHelpers.GetEmpties(False).Slots[Index];
  end else
  begin
    Cache.Value := @Cache.Temp;
  end;

  Result := Cache.Value;
end;

class function TSyncAllocator<TNext,THeader,TValue>.GetEmptiesSlot: PSyncPointer;
var
  HighAddress: NativeInt;
begin
  HighAddress := NativeInt(@FEmptiesSlotCache[TSyncHelpers.TEmpties.ALIGN - 1]);
  Result := TSyncHelpers.PEmptiesSlotCache(HighAddress and -TSyncHelpers.TEmpties.ALIGN).Value;
  if (Result = nil) then
    Result := InternalGetEmptiesSlot;
end;

class function TSyncAllocator<TNext,THeader,TValue>.AddEmptiesPool: Pointer;
var
  Index: NativeUInt;
begin
  if (SizeOf(TNode) = 0) then
    Exit(nil);

  if (TypeInfo(TNext) = TypeInfo(TSyncPointer)) and
    (SizeOf(THeader) + SizeOf(TValue) <= TSyncHelpers.TEmpties.MAX_SIZE) then
  begin
    Index := (SizeOf(THeader) + SizeOf(TValue) + TSyncHelpers.TEmpties.ALIGN - 1) div TSyncHelpers.TEmpties.ALIGN;
    Result := TSyncHelpers.GetEmpties(True).AllocPool(SizeOf(TNode), Index);
  end else
  if (SizeOf(TNode) <= TSyncHelpers.TEmpties.MAX_SIZE) then
  begin
    Index := (SizeOf(TNode) + TSyncHelpers.TEmpties.ALIGN - 1) div TSyncHelpers.TEmpties.ALIGN;
    Result := TSyncHelpers.GetEmpties(False).AllocPool(SizeOf(TNode), Index);
  end else
  begin
    Result := TSyncHelpers.TEmpties.AllocPool(EmptiesSlot^, SizeOf(TNode), TSyncHelpers.TEmpties.LARGE_COUNT);
  end;
end;

class function TSyncAllocator<TNext,THeader,TValue>.New: Pointer;
var
  Node: PNode;
begin
  if (SizeOf(TNode) = 0) then
    Exit(nil);

  Node := GetEmptiesSlot^.Pop;
  if (Node = nil) then
    Node := AddEmptiesPool;

  System.Initialize(Node.Next);
  System.Initialize(Node.Header);
  System.Initialize(Node.Value);

  Result := Node;
end;

class procedure TSyncAllocator<TNext,THeader,TValue>.Dispose(const X: Pointer);
var
  Node: PNode;
begin
  Node := X;

  System.Finalize(Node.Next);
  System.Finalize(Node.Header);
  System.Finalize(Node.Value);

  EmptiesSlot^.Push(Node);
end;


{ TSyncStack<T> }

constructor TSyncStack<T>.Create;
begin
  inherited;

end;

destructor TSyncStack<T>.Destroy;
begin
  Clear;
  inherited;
end;

function TSyncStack<T>.GetEmpty: Boolean;
begin
  {$ifdef CPUX64}
    Result := (FHead.F.VNative and TSyncPointer.X64_SYNCPTR_MASK = 0);
  {$else .CPUARM64}
    Result := (FHead.F.Value = nil);
  {$endif}
end;
          (*
function TSyncStack<T>.GetCount: Integer;
begin
  if (not FCounting) then
  begin
    raise Exception.Create('Stack has no counting');
  end else
  begin
    Result := FCount;
  end;
end;      *)

procedure TSyncStack<T>.Push(const Value: T);
var
  Node: PNode;
begin
  Node := TSyncAllocator<Pointer,TNothing,T>.New;
  Node.Value := Value;
  FHead.Push(Node);

//  if (FCounting) then
//    AtomicIncrement(FCount);
end;

function TSyncStack<T>.Pop(var Value: T): Boolean;
var
  Node: PNode;
begin
  Node := FHead.Pop;
  if (not Assigned(Node)) then
    Exit(False);

  Value := Node.Value;

  //if (FCounting) then
  //  AtomicDecrement(FCount);

  TSyncAllocator<Pointer,TNothing,T>.Dispose(Node);
  Result := True;
end;

procedure TSyncStack<T>.Clear;
var
  i, ItemsCount: Integer;
  First, Last, Node: PNode;
begin
  First := FHead.PopList;
  if (not Assigned(First)) then
    Exit;

  Last := First;
  ItemsCount := 1;
  repeat
    Node := Node.Next;
    if (not Assigned(Node)) then Break;
    Inc(ItemsCount);
    Last := Node;
  until (False);

  //if (FCounting) then
  //  AtomicDecrement(FCount, ItemsCount);

  Node := First;
  for i := 0 to ItemsCount - 1 do
  begin
    System.Finalize(Node.Value);
    Node := Node.Next;
  end;

  TSyncAllocator<Pointer,TNothing,T>.GetEmptiesSlot^.PushList(First, Last);
end;

procedure TSyncStack<T>.Push(const Values: array of T);
var
  i: Integer;
  First, Last, Node: PNode;
begin
  if (High(Values) < 0) then
    Exit;

  First := TSyncAllocator<Pointer,TNothing,T>.New;
  Last := First;
  for i := 1 to Length(Values) - 1 do
  begin
    Last.Next := TSyncAllocator<Pointer,TNothing,T>.New;
    Last := Last.Next;
  end;

  Node := First;
  for i := High(Values) downto Low(Values) do
  begin
    Node.Value := Values[i];
    Node := Node.Next;
  end;

  FHead.PushList(First, Last);

  //if (FCounting) then
  //  AtomicIncrement(FCount, Length(Values));
end;

function TSyncStack<T>.PopAll: TArray<T>;
var
  i, ItemsCount: Integer;
  First, Last, Node: PNode;
begin
  First := FHead.PopList;
  if (not Assigned(First)) then
    Exit(nil);

  Last := First;
  ItemsCount := 1;
  repeat
    Node := Last.Next;
    if (not Assigned(Node)) then Break;
    Inc(ItemsCount);
    Last := Node;
  until (False);

  //if (FCounting) then
  //  AtomicDecrement(FCount, ItemsCount);

  SetLength(Result, ItemsCount);
  Node := First;
  for i := 0 to ItemsCount - 1 do
  begin
    Result[i] := Node.Value;
    System.Finalize(Node.Value);
    Node := Node.Next;
  end;

  TSyncAllocator<Pointer,TNothing,T>.GetEmptiesSlot^.PushList(First, Last);
end;


{ TSyncQueue<T> }

constructor TSyncQueue<T>.Create;
begin
  inherited;

end;

destructor TSyncQueue<T>.Destroy;
begin
  Clear;
  inherited;
end;

function TSyncQueue<T>.GetEmpty: Boolean;
begin
  {$ifdef CPUX64}
    Result := (FTail.F.VNative and TSyncPointer.X64_SYNCPTR_MASK = 0);
  {$else .CPUARM64}
    Result := (FTail.F.Value = nil);
  {$endif}
end;

function TSyncQueue<T>.Enqueue(const Value: T): Boolean;
var
  Node: PNode;
  Tail: TSyncPointer;
begin
  // ����� ����
  Node := TSyncAllocator<Pointer,TNothing,T>.New;
  Node.Value := Value;
  Node.Next := nil;

  // ��������� FTail �� ����
  Tail := FTail.AtomicExchange(Node);
  (*repeat
    Tail := FTail.Copy;
  until (FTail.AtomicCmpExchange(Node, Tail)); *)

  // �������������� Next ��������� ���� ��� ��� FHead
  if (Tail.Empty) then
  begin
    {$ifdef ENQUEUE_SLEEP}
      Sleep(3);
    {$endif}
    //FHead.Fill(Node);
    FHead.AtomicExchange(Node);
  end else
  begin
    PNode(Tail.Value).Next := Node;
  end;

  Result := True;
end;

function TSyncQueue<T>.Dequeue(var Value: T): Boolean;
var
  Head, Tail: TSyncPointer;
  Next: Pointer;
begin
  repeat
    Tail := FTail.Copy;
    Head := FHead.Copy;

    if (Head.Empty) then
    begin
      // ������� ������ ���� Tail ������ - �������
      // ����� ���� ����� Tail, � Head ��� �� ������������������ ���������� ���������
      if (Tail.Empty) then
        Exit(False);
    end else
    begin
      // �������� FHead �� Next, ��� ������������� �������� FTail
      // Next ������ ���� ��� �� ��������������� (���) ��� ��������� (�������� FTail)
      Next := PNode(Head.Value).Next;
      if (Next = nil) then
      begin
        if (Head.Value = Tail.Value) then
        begin
          // ��������� ������������ ������� - �������� FTail
          // � �� ����������� �������� FHead
          if (FTail.AtomicCmpExchange(nil, Tail)) then
          begin
            FHead.AtomicCmpExchange(nil, Head);
            Break;
          end;
        end else
        begin
          // Next �� ���������������
          // ������ ���
        end;
      end else
      begin
        // Next �������� �������� ��������� �� ��������� ���� - ����� ��� � FHead
        if (FHead.AtomicCmpExchange(Next, Head)) then
          Break;
      end;
    end;
  until (False);

  // ���������
  Value := PNode(Head.Value).Value;
  TSyncAllocator<Pointer,TNothing,T>.Dispose(Head.Value);
  Result := True;
end;

procedure TSyncQueue<T>.Clear;
begin
  // ToDo
end;


end.
