program Project;

{$APPTYPE CONSOLE}

//{$R *.res}

uses
  Winapi.Windows,
  System.SysUtils,
  System.Classes,
  LockFree in 'LockFree.pas';

type
  // �������� ������ ��������: ������, ������� (����������), ���������
  TQueueJob = (qjNone, qjEmission, qjDone);

  // �����, ����������� �������� � �������
  TEmissionThread = class(TThread)
  private
    const
      COUNT = 2;
  protected
    procedure Execute; override;
  end;

  // �����, "��������������" ��������, ����������� �� �������
  TProcessingThread = class(TThread)
  private
    const
      COUNT = 2;
  protected
    procedure Execute; override;
  end;


  {.$define LOCKSTACK}
  TQueue =
    {$ifdef LOCKSTACK}
    class(TSyncStack<Integer>)
      function Enqueue(const Value: Integer): Boolean;
      function Dequeue(var Value: Integer): Boolean;
    {$else}
    class(TSyncQueue<Integer>)
    {$endif}
  end;

const
  ITEMS_COUNT = 10 * 1000 * 1000; // 10 ���. ���������


var
  i: Integer;
  QueueJob: TQueueJob;
  Index: Integer;
  Threads: array[0..TEmissionThread.COUNT + TProcessingThread.COUNT - 1] of TThread;
  Handles: array[Low(Threads)..High(Threads)] of THandle;
  Queue: TQueue;
  Items: array[1..ITEMS_COUNT] of Integer{Byte};


procedure PauseCPU;
asm
  pause
end;

procedure TEmissionThread.Execute;
var
  Value: Integer;
begin
  while (not Terminated) and (QueueJob <= qjEmission) do
  begin
    if (QueueJob = qjNone) then
    begin
      PauseCPU;
      Continue;
    end;

    // ��������� �������� � �������
    Value := AtomicIncrement(Index);
    if (Value <= ITEMS_COUNT) then
    begin
      Project.Queue.Enqueue(Value);
    end else
    begin
      // ������ ��� ��������� �� ���������� - ������ ���������
      AtomicCmpExchange(Byte(QueueJob), Byte(qjDone), Byte(qjEmission));
    end;
  end;
end;

procedure TProcessingThread.Execute;
var
  Value: Integer;
  Counter: Integer;
begin
  while (not Terminated) do
  begin
    if (QueueJob = qjNone) then
    begin
      PauseCPU;
      Continue;
    end;

    if (Project.Queue.Dequeue(Value)) then
    begin
      if (Value < Low(Items)) or (Value > High(Items)) then
      begin
        Writeln('����� �� ���������� �������: ', Value);
      end else
      begin
        Counter := AtomicIncrement(Items[Value]);
        if (Counter > 1) then
          Writeln('������� ', Value, ' ����� ������ ������ ����');
      end;
    end else
    begin
      if (QueueJob = qjDone) then
        Break;
    end;
  end;
end;

{$ifdef LOCKSTACK}
function TQueue.Enqueue(const Value: Integer): Boolean;
begin
  Push(Value);
  Result := True;
end;

function TQueue.Dequeue(var Value: Integer): Boolean;
begin
  Result := Pop(Value);
end;
{$endif}


begin
  try
    Queue := TQueue.Create;
    try
      // �������������� ������: ������������/��������������
      for i := 0 to TEmissionThread.COUNT - 1 do
        Threads[i] := TEmissionThread.Create;
      for i := 0 to TProcessingThread.COUNT - 1 do
        Threads[TEmissionThread.COUNT + i] := TProcessingThread.Create;

      // ������
      for i := Low(Threads) to High(Threads) do
        Handles[i] := Threads[i].Handle;

      // "�����" ���������
      QueueJob := qjEmission;

      // ���������� ����� ���������
      WaitForMultipleObjects(Length(Handles), @Handles, True, INFINITE);

      // ���������, ���� �� ����������� ��������
      for i := Low(Items) to High(Items) do
      if (Items[i] = 0) then
        Writeln('������� ', i, ' �������');
    finally
      Queue.Free;
    end;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;

  if (ParamStr(1) <> '-nowait') then
  begin
    Writeln;
    Write('Press Enter to quit');
    Readln;
  end;
end.
